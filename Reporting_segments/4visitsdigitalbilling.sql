
/*HIT
Page Name (v37) contains resi|selfservice|myaccount|billing
- OR -
Page Name (v37) = resi|selfservice|myaccountapp|Digital Bill
- OR -
Page Name (v37) = resi|selfservice|myaccountapp|Bill History
- OR -
Page Name (v37) = resi|selfservice|myaccountapp|View Bill PDF
- OR -
Page Name (v37) = resi|selfservice|myaccountapp|Payment Confirmation
- OR -
Page Name (v37) = resi|selfservice|myaccountapp|Bill Delivery Paper Confirmation*/


select count(distinct post_evar37)
from meld.digital_modeling.omn_activity_basetable_dotcom
where date_time>= '2020-09-01' 
and date_time <='2020-09-10'
and post_evar37 like '%resi|selfservice|myaccount|billing%'
             or post_evar37 like '%resi|selfservice|myaccountapp|Digital Bill%'
             or post_evar37 like '%resi|selfservice|myaccountapp|Bill History%'
             or post_evar37 like '%resi|selfservice|myaccountapp|View Bill PDF%'
             or post_evar37 like '%resi|selfservice|myaccountapp|Payment Confirmation%'
              or post_evar37 like '%resi|selfservice|myaccountapp|Bill Delivery Paper Confirmation%'


/* not matching 
adobeworkspace = 4,475,611  
 meld = 4,637
 
 20% of data is missing */
