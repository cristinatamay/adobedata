with table1 as (
select 
*,
Concat(',',post_event_list,',')like '%,256,%' as loginVer
from meld.digital_modeling.omn_activity_basetable_dotcom
where date_time>= '2020-09-01' 
and date_time <='2020-09-10'
),
table2 as ( 
select * from table1
where post_evar64 is not null 
or loginVer = true 
),
table3 as (
select * from table2
where post_evar37 like '%help & support%'
             or post_evar37 like '%itg%'
             or post_evar37 like '%myaccount%'
             or post_evar37 like '%troubleshoot%'
             or post_evar37 like 'resi|selfservice|myaccountapp|%'
             or post_evar30 = 'native'
)
select count ( distinct post_evar64) as Uniquevisits 
from table3

/* not matching 
adobeworkspace = 10,037,385 
 meld = 6,335,896
 
 20% of adobe data missing */