with table1 as (
select 
*,
Concat(',',post_event_list,',')like '%,256,%' as loginVer
from meld.digital_modeling.omn_activity_basetable_dotcom
where date_time>= '2020-09-01' 
and date_time <='2020-09-10'
),
table2 as ( 
select * from table1
where post_evar64 is not null 
or loginVer = true 
),
table3 as (
select * from table2
where post_evar37 like '%help & support%'
             or post_evar37 like '%itg%'
             or post_evar37 like '%myaccount%'
             or post_evar37 like '%troubleshoot%'
             and post_evar37 not like  '%myaccountapp%'
             and post_evar30 != 'native'
)
select count ( distinct post_evar64) as uniquevisits 
from table3

/*  matching 
 adobeworkspace = 6,434,366
  meld=  6,335,847
  
  1.5% of data missing */