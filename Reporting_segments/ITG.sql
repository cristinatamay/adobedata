/*
"HIT
Page Name (v37) starts with resi|selfservice|help & support|troubleshooting|itg"
*/

select count(distinct *)
from meld.digital_modeling.omn_activity_basetable_dotcom
where date_time>= '2020-09-01' 
and date_time <='2020-09-10'
and post_evar37 like 'resi|selfservice|help & support|troubleshooting|itg%'