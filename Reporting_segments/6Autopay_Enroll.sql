/*"HIT
HIT
Page Name (v37) = resi|selfservice|myaccount|billing|payment|review
- AND -
Self Service Transaction (v89) = billing:autopay:signup
- OR -
HIT
Page Name (v37) = resi|selfservice|myaccount|billing|autopay|edit
- AND -
Self Service Transaction (v89) = billing:autopay:signup"*/

with table1 as (
select 
*,
from meld.digital_modeling.omn_activity_basetable_dotcom
where date_time>= '2020-09-01' 
and date_time <='2020-09-10'
),
table2 as ( 
select * from table1
where
 ( post_evar37 like '%resi|selfservice|myaccount|billing|payment|review%'
 And post_evar_89= 'billing:autopay:signup') 
 OR 
(post_evar37 like '%resi|selfservice|myaccount|billing|autopay|edit%'
 And post_evar_89= 'billing:autopay:signup')
)

 select * from table2 
