/*"HIT
Bill Pay (e7) exists
- OR -
Scheduled Single Pay (e21) exists
- OR -
Page Name (v37) = resi|selfservice|myaccountapp|Payment Confirmation
- OR -
Page Name (v37) = resi|selfservice|myaccountapp|Scheduled Payment Rescheduled"*/

with table1 as (
select 
*,
Concat(',',post_event_list,',')like '%,206,%' as billPay,
Concat(',',post_event_list,',')like '%,220,%' as scheduled_Single_Pay
from meld.digital_modeling.omn_activity_basetable_dotcom
where date_time>= '2020-09-01' 
and date_time <='2020-09-10'
),
table2 as ( 
select * from table1
where billPay is not null
 or scheduled_Single_Pay is not null
 or post_evar37 like '%resi|selfservice|myaccountapp|Payment Confirmation%'
 or post_evar37 like '%resi|selfservice|myaccountapp|Scheduled Payment Rescheduled%'

)
select count ( distinct post_evar64) as uniquevisits_5
from table2